﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Used to create and edit Booking objects, depending on State enum passed through and overloaded constructor
    // Last Edited: 05/12/2016
    public partial class BookingWindow : Window
    {
        private Customer cust;
        private IFormatProvider culture = new System.Globalization.CultureInfo("en", true);
        private Booking b;
        private BookingWindow _instance;
        private State state;

        // Public contructor used to instantiate a new instance of BookingWindow
        public BookingWindow(Customer c, State state)
        {
            InitializeComponent();
            this.cust = c;
            this.state = state;
            if (state == State.CREATE) 
            {
                b = new Booking(cust);
                refreshBookingWindow();
            }
                       
            _instance = this;
        }
        //I overload the constructor here to accept new variables and arguments, namely Booking if user is editing an existing Booking
        public BookingWindow(Booking b, Customer c, State state) : this(c, state)
        {
            if (state == State.EDIT)
            {
                this.b = b;
                refreshBookingWindow();
                BookingWindowCreateBookingBtn.Content = ("Edit Booking");
            }
        }
        
        // Click event for Create Booking Button
        private void BookingWindowCreateBookingBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((DateTime)BookingWindowStartDateDtpckr.SelectedDate != null && (DateTime)BookingWindowEndDateDtpckr.SelectedDate != null)
                {

                    try
                    {
                        DateTime arr = (DateTime)BookingWindowStartDateDtpckr.SelectedDate;
                        DateTime dep = (DateTime)BookingWindowEndDateDtpckr.SelectedDate;

                        b.ArrivalDate = arr.Date;
                        b.DepartureDate = dep.Date;

                        if (BookingWindowCarhireChckbx.IsChecked.Value && BookingWindowCarhireDriverNameTxt.Text != "" && BookingWindowCarhireDriverNameTxt.Text != null)
                        {
                            b.CarStartDate = (DateTime)BookingWindowCarhireStartDateDtpckr.SelectedDate;
                            b.CarEndDate = (DateTime)BookingWindowCarhireEndDateDtpckr.SelectedDate;
                            b.CarDriverName = BookingWindowCarhireDriverNameTxt.Text;
                            b.IsCarHire = BookingWindowCarhireChckbx.IsChecked.Value;
                        }
                        else b.IsCarHire = false;


                        if (BookingWindowBreakfastChckbx.IsChecked.Value)
                        {
                            if (BookingWindowBreakfastTxt.Text != "" && BookingWindowBreakfastTxt.Text != null)
                                b.BreakfastSpecs = BookingWindowBreakfastTxt.Text;
                            b.IsBreakfast = BookingWindowBreakfastChckbx.IsChecked.Value;
                        }
                        else b.IsBreakfast = BookingWindowBreakfastChckbx.IsChecked.Value; 


                        if (BookingWindowDinnerChckbx.IsChecked.Value)
                        {
                            if (BookingWindowDinnerTxt.Text != "" && BookingWindowDinnerTxt.Text != null)
                                b.DinnerSpecs = BookingWindowDinnerTxt.Text;
                            b.IsDinner = BookingWindowDinnerChckbx.IsChecked.Value;
                        }
                        else b.IsDinner = BookingWindowDinnerChckbx.IsChecked.Value; 

                        if (state == State.CREATE)
                            cust.BookingList.Add(b);
                        MainWindow.getInstance().refreshCustCmbx();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        // Click event for Add Guest Button
        private void BookingWindowAddGuestBtn_Click(object sender, RoutedEventArgs e)
        {
            if (b != null)
            {
                if (b.Guestlist.Count < 4)
                    new GuestWindow(b, this, State.CREATE).Show();
                else MessageBox.Show("Guests cannot exceed 4 per booking.");
            }
        }
        // This returns an instance of this Booking Window, used to refresh Guest count from the Guest Window
        public BookingWindow getInstance()
        {
            return this._instance;
        }

        // This method is used to refresh the customer and booking information in the current booking window, which is subject to change dependant on Guests being added from Guest Window
        public void refreshBookingWindow()
        {
            BookingWindowCustInfoTextLbl.Content = (cust.CustRefNum + "(" + cust.BookingList.Count + "): " + cust.Name);
            BookingWindowBookingInfoLbl.Content = ("Guests in this booking: " + b.Guestlist.Count + "/4");

            if (state == State.EDIT)
            {               
                BookingWindowStartDateDtpckr.SelectedDate = b.ArrivalDate;
                BookingWindowEndDateDtpckr.SelectedDate = b.DepartureDate;

                if (b.IsDinner)
                {
                    BookingWindowDinnerChckbx.IsChecked = b.IsDinner;
                    BookingWindowDinnerTxt.IsEnabled = true;
                    BookingWindowDinnerTxt.Text = b.DinnerSpecs;
                }
                if (b.IsBreakfast)
                {
                    BookingWindowBreakfastChckbx.IsChecked = b.IsBreakfast;
                    BookingWindowBreakfastTxt.IsEnabled = true;
                    BookingWindowBreakfastTxt.Text = b.BreakfastSpecs;
                }
               
                if (b.IsCarHire)
                {                   
                    BookingWindowCarhireChckbx.IsChecked = b.IsCarHire;
                    BookingWindowCarhireStartDateDtpckr.IsEnabled = true;
                    BookingWindowCarhireEndDateDtpckr.IsEnabled = true;
                    BookingWindowCarhireDriverNameTxt.IsEnabled = true;
                    BookingWindowCarhireStartDateDtpckr.SelectedDate = b.CarStartDate;
                    BookingWindowCarhireEndDateDtpckr.SelectedDate = b.CarEndDate;
                    BookingWindowCarhireDriverNameTxt.Text = b.CarDriverName;
                } 
            }

            if (b.Guestlist.Count > 0)
            {
                BookingWindowGuestCmbx.IsEnabled = true;
                BookingWindowEditGuestBtn.IsEnabled = true;
                BookingWindowDeleteGuestBtn.IsEnabled = true;
                BookingWindowGuestCmbx.Items.Clear();
                foreach (Guest g in b.Guestlist)
                {
                    BookingWindowGuestCmbx.Items.Add(g.Name + ", " + g.Age);
                }
                BookingWindowGuestCmbx.SelectedIndex = b.Guestlist.Count - 1;
            }
            else
            {
                BookingWindowGuestCmbx.Items.Clear();
                BookingWindowDeleteGuestBtn.IsEnabled = false;
                BookingWindowGuestCmbx.IsEnabled = false;
                BookingWindowEditGuestBtn.IsEnabled = false;
            }
            if (b.Guestlist.Count == 4)
                BookingWindowAddGuestBtn.IsEnabled = false;
            else BookingWindowAddGuestBtn.IsEnabled = true;
        }

        // Click event for Edit Guest Button
        private void BookingWindowEditGuestBtn_Click(object sender, RoutedEventArgs e)
        {
            if ((string)BookingWindowGuestCmbx.SelectedItem != "" && b.Guestlist.Count > 0)
            {
                new GuestWindow((Guest)b.Guestlist[BookingWindowGuestCmbx.SelectedIndex], b, this, State.EDIT).Show();
            }
        }

        // Click event for Delete Guest Button
        private void BookingWindowDeleteGuestBtn_Click(object sender, RoutedEventArgs e)
        {
            if (b.Guestlist.Count > 0)
            {
                b.Guestlist.Remove(b.Guestlist[BookingWindowGuestCmbx.SelectedIndex]);
                refreshBookingWindow();
            }
        }

        // Click event for Car Hire Checkbox 
        private void BookingWindowCarhireChckbx_Checked(object sender, RoutedEventArgs e)
        {
            if (BookingWindowCarhireChckbx.IsChecked.Value)
            {
                BookingWindowCarhireDriverNameTxt.IsEnabled = true;
                BookingWindowCarhireStartDateDtpckr.IsEnabled = true;
                BookingWindowCarhireEndDateDtpckr.IsEnabled = true;
            }
            else
            {
                BookingWindowCarhireDriverNameTxt.IsEnabled = false;
                BookingWindowCarhireStartDateDtpckr.IsEnabled = false;
                BookingWindowCarhireEndDateDtpckr.IsEnabled = false;
            }
        }
        // Click event for Dinner Checkbox
        private void BookingWindowDinnerChckbx_Checked(object sender, RoutedEventArgs e)
        {
            if (BookingWindowDinnerChckbx.IsChecked.Value)
                BookingWindowDinnerTxt.IsEnabled = true;
            else BookingWindowDinnerTxt.IsEnabled = false;
        }

        // Click event for Breakfast Checkbox
        private void BookingWindowBreakfastChckbx_Checked(object sender, RoutedEventArgs e)
        {
            if (BookingWindowBreakfastChckbx.IsChecked.Value)
                BookingWindowBreakfastTxt.IsEnabled = true;
            else BookingWindowBreakfastTxt.IsEnabled = false;
        }

    }
}

