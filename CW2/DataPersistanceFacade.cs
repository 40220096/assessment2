﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Used to Serialize and Deserialize data, aka data persistance.
    // This class is an implementation of the Facade design pattern. It is used to enforce data persistance and hides the messy nature of (de)serialization.
    // Last Edited: 08/12/2016
    public static class DataPersistanceFacade
    {
     
        //This method is part of the facade to hide the true nature and complications of the serialization code.
        public static void Serialize(Object o)
        {
            SerializeData.ToXML(o);      
        }
        //This method is part of the facade to hide the true nature and complications of the deserialization code.
        public static void Deserialize()
        {        
            DeserializeData.LoadXML();          
        }

    }
}
