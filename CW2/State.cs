﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Used to define the use of a window, whether I am editing or creating a new object within.
    // Last Edited: 05/12/2016
    [Serializable]
    public enum State
    {
        CREATE,
        EDIT
    }
}
