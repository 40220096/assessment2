﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CW2
{
    /// <summary>
    /// Interaction logic for EditBooking.xaml
    /// </summary>
    public partial class EditBooking : Window
    {
        private IFormatProvider culture = new System.Globalization.CultureInfo("en", true);
        private Booking b;
        public EditBooking(Booking b)
        {
            InitializeComponent();
            this.b = b;

            //Assigning values to the labels and such on the EditBookingWindow
            EditBookingWindowCustInfoLbl.Content = ("Booking made by Customer: (" + b.Cust.CustRefNum + ") " + b.Cust.Name);
            EditBookingWindowInfoLbl.Content = ("Edit Booking: " + b.BookRefNum);
            EditBookingWindowArrivalDateTxt.Text = (b.ArrivalDate.ToShortDateString());
            EditBookingWindowDepartureDateTxt.Text = (b.DepartureDate.ToShortDateString());
        }

        private void EditBookingWindowEditBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime arr = DateTime.ParseExact(EditBookingWindowArrivalDateTxt.Text, "dd/MM/yyyy", culture);
                DateTime dep = DateTime.ParseExact(EditBookingWindowDepartureDateTxt.Text, "dd/MM/yyyy", culture);

                b.ArrivalDate = arr.Date;
                b.DepartureDate = dep.Date;
                this.Close();
                MainWindow.getInstance().refreshCustCmbx();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
