﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: To create Guest objects and store the variables within. Inherits from Person.
    // Last Edited: 05/12/2016
    [Serializable]
    public class Guest : Person
    {
        private int age;
        private string passportNumber;
        private Booking booking;

        //Private constructor necessary for XML Serialization, private so that it cannot be used without booking Argument
        private Guest()
        {

        }

        //Public constructor for creating new Guest objects
        public Guest(Booking b)
        {
            this.booking = b;
        }

        //Property that returns Age and validates the setter
        public int Age
        {
            get { return this.age; }
            set
            {
                if (value >= 0 && value <= 110)
                    this.age = value;
                else throw new ArgumentException("Age must be between 0 and 110");
            }
        }
        //Property that returns passport number and validates the setter
        public string PassportNumber
        {
            get { return this.passportNumber; }
            set
            {
                if (value.Length < 11 && value != null && value != "")
                    this.passportNumber = value;
                else throw new ArgumentException("Passport number length either greater than 10 or null");
                }
        }
        //Property that returns Booking, no setter.
        public Booking Booking
        {
            //Hoping I won't need a setter for this
            get { return this.booking; }
        }




    }
}
