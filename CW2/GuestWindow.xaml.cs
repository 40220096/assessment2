﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Guest Window is used to create and edit Guest Objects dependant on State enum and overloaded constructors.
    // Last Edited: 05/12/2016
    public partial class GuestWindow : Window
    {
        private Booking b;
        private BookingWindow bw;
        private State state;
        private Guest g = null;
        //Constructor used for creating new guests
        public GuestWindow(Booking b, BookingWindow bw, State s)
        {
            InitializeComponent();
            this.b = b;
            this.bw = bw;
            this.state = s;
            GuestWindowCustomerInfoLbl.Content = "";
            GuestWindowCustomerInfoLbl.Content = ("Add guest to customer: "+b.Cust.Name + " (" + b.Cust.CustRefNum + ")");
            GuestWindowBookingInfoLbl.Content = ("Booking Info: Ref #: " + b.BookRefNum + ", current guest(s): " + b.Guestlist.Count + "/4");
            if(state == State.CREATE)
                g = new Guest(b);
        }
        //Overloading constructor to accept a Guest object as well, enables editing guests
        public GuestWindow(Guest g, Booking b, BookingWindow bw, State s)
            : this(b, bw, s) 
        {
            this.g = g;
            if (state == State.EDIT)
            {
                GuestWindowAddGuestBtn.Content = ("Edit Guest");
                GuestWindowGuestAgeTxt.Text = g.Age+"";
                GuestWindowGuestNameTxt.Text = g.Name;
                GuestWindowPassportNumberTxt.Text = g.PassportNumber;
            }
        }

        //Button click event for Add Guest Button
        private void GuestWindowAddGuestBtn_Click(object sender, RoutedEventArgs e)
        {
            if (GuestWindowGuestNameTxt.Text != "" && GuestWindowGuestAgeTxt.Text != "" && GuestWindowPassportNumberTxt.Text != "")
            {
                try
                {
                    
                    g.Age = int.Parse(GuestWindowGuestAgeTxt.Text);
                    g.Name = GuestWindowGuestNameTxt.Text;
                    g.PassportNumber = GuestWindowPassportNumberTxt.Text;
                    if(state == State.CREATE)
                        b.Guestlist.Add(g);
                    if (bw != null)
                        bw.getInstance().refreshBookingWindow();
                    this.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            else MessageBox.Show("All fields must have a value.");
        }
    }
}
