﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Invoice Window, used to create an invoice for booking, shows different costs based on different factors and calculations.
    // Last Edited: 05/12/2016
    public partial class InvoiceWindow : Window
    {
        private Booking b;

        //Main constructor for InvoiceWindow, summons cost from object, temporarily calculates some individual costs and sets label contents.
        public InvoiceWindow(Booking b)
        {
            InitializeComponent();
            this.b = b;

            if (b.IsBreakfast)
                InvoiceWindowBreakfastExtraLbl.Content = ("Breakfast has been selected, will cost a total of: "+b.getCost()[3]);
            if(b.IsDinner)
                InvoiceWindowDinnerExtraLbl.Content = ("Dinner has been selected, will cost a total of: " + b.getCost()[2]);
            if (b.IsCarHire)
            {
                int carhireduration = (b.CarEndDate - b.CarStartDate).Days;
                InvoiceWindowCarHireExtraLbl.Content = ("Car hire has been selected for " + carhireduration + " day(s), costing: " + b.getCost()[4]);
            }
            InvoiceWindowTotalCostLbl.Content = ("Total cost of booking: £"+b.getCost()[0]);


            InvoiceWindowInfoLbl.Content = ("Invoice for booking: (" + b.BookRefNum + "), belonging to customer: (" + b.Cust.CustRefNum + ") "+b.Cust.Name);
            InvoiceWindowNightsLbl.Content = ("Nights in stay: "+b.getCost()[6]+", at a cost of: £"+b.getCost()[5]+ " ("+(b.getCost()[5]/b.getCost()[6])+" per night).");
        }

        private void InvoiceWindowCloseBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
