﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Customer Window, used for creating and editing Customer objects dependant on State enum and overloaded constructors.
    // Last Edited: 05/12/2016
    public partial class CustomerWindow : Window
    {
        private Customer c=null;
        private State state;
        //Simple constructor, used for opening this window when attempting to create a new customer
        public CustomerWindow(State state)
        {
            InitializeComponent();
            this.state = state;
        }
        // Overloading the initial customer, used to pass in existing Customer object if editing the customer and not creating a new one.
        public CustomerWindow(Customer c, State state) : this (state)
        {
            this.c = c;
            if (state == State.EDIT)
            {
                CustomerWindowCreateBtn.Content = ("Edit Customer");
                CustomerWindowNameTxt.Text = c.Name;
                CustomerWindowAddressTxt.Text = c.Address;
            }


        }

        // Button click event for Create/Edit Customer button
        private void CustomerWindowCreateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CustomerWindowAddressTxt.Text != "" && CustomerWindowNameTxt.Text != "")
            {
                try
                {
                    if (state == State.CREATE)
                    {
                        c = new Customer();
                        c.CustRefNum = RefNumSingleton.getInstance().genCustRef();
                    }                     

                    c.Name = CustomerWindowNameTxt.Text;
                    c.Address = CustomerWindowAddressTxt.Text;
                    

                    if(state == State.CREATE)
                        MainWindow.clist.Add(c);
                    
                    MainWindow.getInstance().refreshCustCmbx();
                    this.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}
