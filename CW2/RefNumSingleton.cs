﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Used to store and generate unique customer and booking reference numbers, this class mimics the Singleton Design Pattern.
    // Singleton is used to enforce data consistancy across my classes.
    // Last Edited: 05/12/2016
    public class RefNumSingleton
    {
        private int custRef = MainWindow.clist.Count;
        private int bookingRef = 0;
        private bool isBookRefRefreshed = false;
        private static RefNumSingleton _instance = new RefNumSingleton();


        //Made constructor private, so new instance of this class cannot be instantiated.
        private RefNumSingleton() { }

        //Method is static and returns static instance of this Singleton class, so that the methods can be accessed.
        public static RefNumSingleton getInstance()
        {
            return _instance;
        }
        //Method returns a new Booking Reference Number, that is unique
        public int genBookRef()
        {
            if (!isBookRefRefreshed)
            {
                foreach (Customer c in MainWindow.clist)
                {
                    foreach (Booking b in c.BookingList)
                        bookingRef++;
                }
                isBookRefRefreshed = true;
            }
            bookingRef++;
            return bookingRef;
        }
        //Method returns a new Customer Reference Number, that is unique
        public int genCustRef()
        {
            custRef++;
            return custRef;
        }


    }
}
