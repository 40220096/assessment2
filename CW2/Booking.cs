﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Booking Class, used to create booking objects and store variables within.
    // Last Edited: 05/12/2016
    [Serializable]
    public class Booking 
    {
        private Customer cust;
        private DateTime arrivalDate;
        private DateTime departureDate;
        private int bookRefNum;
        private bool isCarHire;
        private DateTime carStartDate;
        private DateTime carEndDate;
        private string carDriverName;
        private bool isDinner;
        private string dinnerSpecs;
        private bool isBreakfast;
        private string breakfastSpecs;


        private List<Guest> guestList = new List<Guest>();

        // This method is a private contructor, never to be used but is required for XML serialization to work
        private Booking()
        {

        }
        // This method is a public constructor for this class, ensures that a booking MUST have a customer with which it is associated, ensures it cannot be created without it.
        public Booking(Customer c)
        {
            this.cust = c;
            this.bookRefNum = RefNumSingleton.getInstance().genBookRef();
        }

        //This property returns the Customer associated with this booking
        public Customer Cust
        {
            //Customer HAS to be set in the Constructor, as such no set function necessary/allowed.
            get { return this.cust; }
            //set { this.cust = value; }
        }
        //This property returns  the arrival date for this booking
        public DateTime ArrivalDate
        {
            get { return this.arrivalDate; }
            set { this.arrivalDate = value; }
        }

        //This property returns the departure date for this booking
        public DateTime DepartureDate
        {
            get { return this.departureDate; }
            set { this.departureDate = value; }
        }

        //This property returns the booking reference number for this booking
        public int BookRefNum
        {
            get { return this.bookRefNum; }
            set {
                if (value != 0)
                    this.bookRefNum = value;
                else throw new ArgumentException("Book Reference Number is 0");
            }
        }
        //This property returns a list with the current guests in it
        public List<Guest> Guestlist
        {
            get { return this.guestList; }
        }
        //This property returns whether they have opted for the car hire option or not
        public bool IsCarHire
        {
            get { return this.isCarHire; }
            set { this.isCarHire = value; }
        }
        //This property returns the start date for the car hire
        public DateTime CarStartDate
        {
            get { return this.carStartDate; }
            set { this.carStartDate = value; }
        }

        //This property returns the end date for the car hire
        public DateTime CarEndDate
        {
            get { return this.carEndDate; }
            set { this.carEndDate = value; }
        }

        //This property returns the name of the car driver and enforces the setter to have a value.
        public string CarDriverName
        {
            get { return this.carDriverName; }
            set {
                if (value != "" && value != null)
                    this.carDriverName = value;
                else throw new ArgumentException("Driver either empty or null");
            }
        }
        //This property returns whether they have signed up for dinner extra
        public bool IsDinner
        {
            get { return this.isDinner; }
            set { this.isDinner = value; }
        }
        //This property returns whether they have signed up for the breakfast extra
        public bool IsBreakfast
        {
            get { return this.isBreakfast; }
            set { this.isBreakfast = value; }
        }
        //This property returns the dinner specifications
        public string DinnerSpecs
        {
            get { return this.dinnerSpecs; }
            set {
                if (value != "" && value != null)
                    this.dinnerSpecs = value;
                else throw new ArgumentException("Dinner specs either empty or null");            
            }
        }
        //This property returns the breakfast specifications
        public string BreakfastSpecs
        {
            get { return this.breakfastSpecs; }
            set
            {
                if (value != "" && value != null)
                    this.breakfastSpecs = value;
                else throw new ArgumentException("Dinner specs either empty or null");
            }
        }

        // This method calculates the cost of the entire booking, including nights, extras and extras duration(such as car hire)
        public List<int> getCost()
        {
            int cost = 0;
            int nightAdult = 50;
            int nightChild = 30;
            int nightCost = 0;
            int carHireCost = 50;
            int dinnerCost = 15;
            int breakfastCost = 5;
            int nights = (this.departureDate - this.arrivalDate).Days;
            int carHireDays = 0;
            int extrasCost=0;
            
            
            foreach (Guest g in guestList)
            {
                if (g.Age >= 18)
                {
                    cost += nightAdult * nights;
                    nightCost += nightAdult * nights;
                }
                else
                {
                    cost += nightChild * nights;
                    nightCost += nightChild * nights;
                }
             
            }
            if (isDinner)
            {
                cost += guestList.Count * dinnerCost * nights;
                extrasCost += guestList.Count * dinnerCost * nights;
            }
            if (isBreakfast)
            {
                cost += guestList.Count * breakfastCost * nights;
                extrasCost += guestList.Count * breakfastCost * nights;
            }

            if (isCarHire)
            {

                carHireDays = (this.carEndDate - this.carStartDate).Days;
                cost += carHireCost * carHireDays;
                extrasCost += carHireCost * carHireDays;
            }
            List<int> costList = new List<int>();
            costList.Add(cost);
            costList.Add(extrasCost);
            costList.Add(guestList.Count * dinnerCost * nights);
            costList.Add(guestList.Count * breakfastCost * nights);
            costList.Add(carHireCost * carHireDays);
            costList.Add(nightCost);
            costList.Add(nights);



            return costList;
        }
        // This is a quite cheeky method I use to fix XML circular reference serialization, it sets the objects customer. Only to be used in DataPersistanceFacade with deserialization.
        public void setCustomer(Customer c)
        {
            this.cust = c;
        }

    }
}
