﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace CW2
{
    public static class DataPersistance
    {
        public static string Serialize<T>(this T value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            try
            {
                var xmlserializer = new XmlSerializer(typeof(T));
                var stringWriter = new StringWriter();
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    xmlserializer.Serialize(writer, value);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred", ex);
            }
        }
        public static void ToXML(Object o, String filename)
        {
            XmlSerializer xmlsr = new XmlSerializer(o.GetType());
            TextWriter wr = new StreamWriter(filename);
            //xmlsr.Serialize(wr, o);
            xmlsr.Serialize(Console.Out, o);
            wr.Close();
        }

    }
}
