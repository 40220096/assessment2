﻿#pragma checksum "..\..\GuestWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2DFD3C358D3B3D56DB4DBD163EEBD9AF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace CW2 {
    
    
    /// <summary>
    /// GuestWindow
    /// </summary>
    public partial class GuestWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 6 "..\..\GuestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label GuestWindowCustomerInfoLbl;
        
        #line default
        #line hidden
        
        
        #line 7 "..\..\GuestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label GuestWindowBookingInfoLbl;
        
        #line default
        #line hidden
        
        
        #line 8 "..\..\GuestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label GuestWindowNameLbl;
        
        #line default
        #line hidden
        
        
        #line 9 "..\..\GuestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label GuestWindowPassportNumberLbl;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\GuestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label GuestWindowAgeLbl;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\GuestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox GuestWindowGuestNameTxt;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\GuestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox GuestWindowPassportNumberTxt;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\GuestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox GuestWindowGuestAgeTxt;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\GuestWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GuestWindowAddGuestBtn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/CW2;component/guestwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\GuestWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.GuestWindowCustomerInfoLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.GuestWindowBookingInfoLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.GuestWindowNameLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.GuestWindowPassportNumberLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.GuestWindowAgeLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.GuestWindowGuestNameTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.GuestWindowPassportNumberTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.GuestWindowGuestAgeTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.GuestWindowAddGuestBtn = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\GuestWindow.xaml"
            this.GuestWindowAddGuestBtn.Click += new System.Windows.RoutedEventHandler(this.GuestWindowAddGuestBtn_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

