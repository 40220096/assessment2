﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Person property, used with Customer and Guest classes for Name purpose. Inherits to them as well.
    // Last Edited: 05/12/2016
    [Serializable]
    public class Person
    {
        private string name;

        //Propert that gets and sets Name variable, with validation
        public string Name
        {

            get { return this.name; }

            set
            {
                if (value != "" && value != null)
                    this.name = value;
                else throw new ArgumentException("Name is either empty or null");
            }
        }

    }
}
