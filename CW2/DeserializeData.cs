﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Used to Deserialize data, aka data persistance. It is hidden by a Facade.
    // Last Edited: 08/12/2016
    public static class DeserializeData
    {
        // This method loads and deserializes the data and assigns it to the proper variables.
        public static void LoadXML()
        {
            XmlSerializer xmlsr = new XmlSerializer(typeof(List<Customer>));
            FileStream read = new FileStream("data.xml", FileMode.Open, FileAccess.Read, FileShare.Read);
            MainWindow.clist = (List<Customer>)xmlsr.Deserialize(read);

            foreach (Customer c in MainWindow.clist)
            {
                foreach (Booking b in c.BookingList)
                {
                    b.setCustomer(c);
                }
            }
            MainWindow.getInstance().refreshCustCmbx();
        }

    }
}
