﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Customer class, used to create Customer objects. Inherits from Person.
    // Last Edited: 05/12/2016
    [Serializable]
    public class Customer : Person
    {
        private string address;
        private int custRefNum;
        private List<Booking> bookingList = new List<Booking>();

        // Empty Constructor, used to create Customer objects
        public Customer()
        {

        }

        // Property that gets and sets the address, whilst validating values also.
        public string Address
        {
            get { return this.address; }
            set
            {
                if (value != null && value != "")
                    this.address = value;
                else throw new ArgumentException("Address is either empty or null");
            }
        }
        // Property that gets and sets the Customer Reference Number, whilst validating values also.
        public int CustRefNum
        {
            get { return this.custRefNum; }
            set
            {
                if (value != 0)
                    this.custRefNum = value;
                else throw new ArgumentException("Customer Reference Number is 0");
            }
        }
        // Property that gets and sets the Booking List
        public List<Booking> BookingList
        {
            //Hoping I won't need a set for this
            get { return this.bookingList; }
            
            set { this.bookingList = value; }
        }
    }
}
