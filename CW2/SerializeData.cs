﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Used to Serialize data, aka data persistance. It is hidden by a Facade.
    // Last Edited: 08/12/2016
    public static class SerializeData
    {
        //This Method serializes an object that I pass through to it.
        public static void ToXML(Object o)
        {
            XmlSerializer xmlsr = new XmlSerializer(o.GetType());
            TextWriter wr = new StreamWriter("data.xml");
            xmlsr.Serialize(wr, o);
            wr.Close();
        }
    }
}
