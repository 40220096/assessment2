﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace CW2
{
    // Author: Christopher Bartlett (40220096)
    // Class purpose: Main Window of the program, connects all the other windows together and allows for complete functionality of the program.
    // Last Edited: 05/12/2016
    public partial class MainWindow : Window
    {
        public static List<Customer> clist = new List<Customer>();
        //public static ArrayList blist = new ArrayList();
        private static MainWindow _instance;

        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(MainWindow_Exit);
            _instance = this;
        }

        private void custBtn_Click(object sender, RoutedEventArgs e)
        {
            new CustomerWindow(State.CREATE).Show();

        }
        // This Method refreshes the Customer Combo Box with the updates list of Customers, also toggles activation on some of the MainWindow buttons
        public void refreshCustCmbx()
        {
            if (clist.Count > 0)
            {
                MainWindowCustCmbx.IsEnabled = true;
                MainWindowDeleteCustomerBtn.IsEnabled = true;
                MainWindowCustomerEditBtn.IsEnabled = true;
                MainWindowCustCmbx.Items.Clear();
                foreach (Customer c in clist)
                {
                    MainWindowCustCmbx.Items.Add(c.CustRefNum + "(" + c.BookingList.Count + "): " + c.Name);
                }
                MainWindowCustCmbx.SelectedIndex = clist.Count - 1;
                MainWindowCustCmbx.Items.Refresh();
                checkBookingBtnEnabled();
                updateBookingCmbx();
                
            }
            else 
            {
                MainWindowCustomerEditBtn.IsEnabled = false;
                MainWindowCustCmbx.IsEnabled = false;
                MainWindowDeleteCustomerBtn.IsEnabled = false;
                MainWindowCreateBookingBtn.IsEnabled = false;
                MainWindowCustCmbx.Items.Clear();
            }
        }

        // Returns a static instance of MainWindow so I can access methods within non-statically
        public static MainWindow getInstance()
        {
            return _instance;
        }

        // Button click event for Create Booking Button
        private void MainWindowCreateBookingBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindowCustCmbx.Text != "" && clist.Count > 0)
            {
                foreach (Customer c in clist)
                {
                    if ((c.CustRefNum + "(" + c.BookingList.Count + "): " + c.Name).Equals(MainWindowCustCmbx.Text))
                    {
                        new BookingWindow(c, State.CREATE).Show();
                        continue;
                    }
                }
            }
        }

        private void MainWindowCustCmbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            checkBookingBtnEnabled();
            updateBookingCmbx();


        }

        //Small method that toggles activation on some buttons, dependant on Customer Combo Box text values.
        public void checkBookingBtnEnabled()
        {
            if (MainWindowCustCmbx.Text == "")
                MainWindowCreateBookingBtn.IsEnabled = false;
            else MainWindowCreateBookingBtn.IsEnabled = true;
        }
        // Method that updates the Booking Combo Box, dependant on the Customer that has been selected in the Customer Combo Box. This method is executed when the Customer Combobox Selection Changed Event is fired.
        public void updateBookingCmbx()
        {
            MainWindowBookingCmbx.Items.Clear();
            foreach (Customer c in clist)
            {
                if ((c.CustRefNum + "(" + c.BookingList.Count + "): " + c.Name) == (string)MainWindowCustCmbx.SelectedItem)
                {
                    if (c.BookingList.Count > 0)
                    {
                        MainWindowBookingCmbx.IsEnabled = true;
                        foreach (Booking b in c.BookingList)
                        {
                            MainWindowBookingCmbx.Items.Add(b.BookRefNum + ": " + b.ArrivalDate.ToShortDateString());
                        }
                        MainWindowBookingCmbx.SelectedIndex = c.BookingList.Count - 1;
                    }
                    else MainWindowBookingCmbx.IsEnabled = false;
                }
            }
            if (MainWindowBookingCmbx.Text == "" || (string)MainWindowBookingCmbx.SelectedItem == "")
            {
                MainWindowEditBookingBtn.IsEnabled = false;
                MainWindowDeleteBookingBtn.IsEnabled = false;
                MainWindowBookingInvoiceBtn.IsEnabled = false;
            }
            else
            {
                MainWindowBookingInvoiceBtn.IsEnabled = true;
                MainWindowEditBookingBtn.IsEnabled = true;
                MainWindowDeleteBookingBtn.IsEnabled = true;
            }
            if (!(clist.Count > 0))
                MainWindowCreateBookingBtn.IsEnabled = false;
        }

        // Button click event for the Edit Booking Button
        private void MainWindowEditBookingBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindowBookingCmbx.Text != "" && (string)MainWindowBookingCmbx.SelectedItem != "")
            {
                foreach (Customer c in clist)
                {
                    if ((c.CustRefNum + "(" + c.BookingList.Count + "): " + c.Name) == (string)MainWindowCustCmbx.SelectedItem)
                    {
                        foreach (Booking b in c.BookingList)
                        {
                            if (b.BookRefNum + ": " + b.ArrivalDate.ToShortDateString() == (string)MainWindowBookingCmbx.SelectedItem)
                            {
                                
                                new BookingWindow(b, c, State.EDIT).Show();
                            }
                        }
                    }
                }
            }
        }

        // Button click event for the Delete Booking Button
        private void MainWindowDeleteBookingBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindowBookingCmbx.Text != "" && (string)MainWindowBookingCmbx.SelectedItem != "")
            {
                foreach (Customer c in clist)
                {
                    if ((c.CustRefNum + "(" + c.BookingList.Count + "): " + c.Name) == (string)MainWindowCustCmbx.SelectedItem)
                    {
                        System.Collections.Generic.List<Booking> tempblist = c.BookingList;                       
                        for (int i = 0; i < c.BookingList.Count; i++)
                        {
                            Booking b = (Booking) c.BookingList[i];
                            if (b.BookRefNum + ": " + b.ArrivalDate.ToShortDateString() == (string)MainWindowBookingCmbx.SelectedItem)
                            {
                                if (tempblist.Contains(b))
                                {
                                    c.BookingList.Remove(b);
                                    refreshCustCmbx();
                                }
                            }
                        }
                    }
                }
            }
        }
        // Button click event for the Customer Delete Button
        private void MainWindowDeleteCustomerBtn_Click(object sender, RoutedEventArgs e)
        {
            if (clist.Count > 0)
            {
                for (int i = 0; i < clist.Count; i++)
                {
                    Customer c = (Customer)clist[i];
                    if ((c.CustRefNum + "(" + c.BookingList.Count + "): " + c.Name) == (string)MainWindowCustCmbx.SelectedItem)
                    {
                        if (c.BookingList.Count > 0)
                            MessageBox.Show("Cannot delete customer with existing bookings");
                        else 
                        { 
                            clist.Remove(clist[i]);
                            refreshCustCmbx();
                            
                        }
                    }
                }               
            }
        }
        // Button click event for the Customer Edit Button
        private void MainWindowCustomerEditBtn_Click(object sender, RoutedEventArgs e)
        {
            if (clist.Count > 0)
            {
                foreach (Customer c in clist)
                {
                    if ((c.CustRefNum + "(" + c.BookingList.Count + "): " + c.Name) == (string)MainWindowCustCmbx.SelectedItem)
                    {
                        new CustomerWindow(c, State.EDIT).Show();
                    }
                }
                refreshCustCmbx();
            }
        }

        // Button click event for the Invoice Button
        private void MainWindowBookingInvoiceBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (Customer c in clist)
            {
                if ((c.CustRefNum + "(" + c.BookingList.Count + "): " + c.Name) == (string)MainWindowCustCmbx.SelectedItem)
                {
                    foreach (Booking b in c.BookingList)
                    {
                        if (b.BookRefNum + ": " + b.ArrivalDate.ToShortDateString() == (string)MainWindowBookingCmbx.SelectedItem)
                        {
                            new InvoiceWindow(b).Show();                                                     
                        }
                    }
                }
            }
        }
        // Use of Data Persistance Facade to hide complicated code and methods used to deserialize data, this is executed when the program is loaded.
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (File.Exists("data.xml"))
            {
                DataPersistanceFacade.Deserialize();                
            }
        }
        //Event that is fired when the program is closed, used to serialize my data to XML and save it.
        private void MainWindow_Exit(object sender, EventArgs e)
        {
            try
            {
                DataPersistanceFacade.Serialize(clist);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}