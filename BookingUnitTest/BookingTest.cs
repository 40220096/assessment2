﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CW2;
namespace BookingUnitTest
{
    [TestClass]
    public class BookingTest
    { 
        
        [TestMethod]
        public void Booking_Ref_Should_Throw_Error_If_Incorrect()
        {
            Booking b1 = new Booking(new Customer());
            Assert.AreEqual(b1.BookRefNum, RefNumSingleton.getInstance().genBookRef() - 1, "Booking Ref Number out of sync");           
        }
        [ExpectedException(typeof(InvalidOperationException))]
        [TestMethod]    
        public void Booking_Dates_Arent_Null_Else_Throw_Exception()
        {
            Booking b1 = new Booking(new Customer());
            b1.ArrivalDate = new DateTime(12 / 12 / 2015);
            DateTime? nulldate = null;
            b1.DepartureDate = (DateTime)nulldate;     
            Assert.IsNotNull(b1.ArrivalDate, "Error, arrival date is null");
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void Expect_Customer_Name_Throw_Exception_If_Invalid()
        {
            Customer c = new Customer();
            c.Name = "";
            Booking b = new Booking(c);
        }
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void Expect_Driver_Name_Valid_Else_Throw_Exception()
        {
            Booking b = new Booking(new Customer());
            b.CarDriverName = "";
        }
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void Expect_Breakfast_Specs_Valid_Else_Throw_Exception()
        {
            Booking b = new Booking(new Customer());
            b.BreakfastSpecs = "";
        }
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void Expect_dinner_Specs_Valid_Else_Throw_Exception()
        {
            Booking b = new Booking(new Customer());
            b.DinnerSpecs = "";
        }
    }
}
