﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MainWindowNamespace
{
    /// <summary>
    /// Interaction logic for CustomerWindow.xaml
    /// </summary>
    public partial class CustomerWindow : Window
    {
        public CustomerWindow()
        {
            InitializeComponent();
        }

        private void CustomerWindowCreateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CustomerWindowAddressTxt.Text != "" && CustomerWindowNameTxt.Text != "")
            {
                try
                {
                    Customer c = new Customer();
                    c.FirstName = CustomerWindowNameTxt.Text;
                    c.Address = CustomerWindowAddressTxt.Text;
                    c.CustRefNum = RefNumSingleton.getInstance().genCustRef();
                    MainWindow.clist.Add(c);
                    this.Close();
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}
