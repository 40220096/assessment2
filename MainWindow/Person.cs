﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainWindowNamespace
{
    public class Person
    {
        private string firstName;
        private string lastName;




    public string FirstName {

        get{return this.firstName;}

        set{
            if (value != "" && value != null)
                this.firstName = value;
            else throw new ArgumentException("FirstName is either empty or null");
         }
    }
    public string LastName
    {
        get { return this.lastName; }
        set
        {
            if (value != "" && value != null)
                this.lastName = value;
            else throw new ArgumentException("LastName is either empty or null");
        }
    }



    }
}
