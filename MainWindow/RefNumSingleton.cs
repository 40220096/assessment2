﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainWindowNamespace
{
    class RefNumSingleton
    {
        private static int custRef = 0;
        private static int bookingRef = 0;
        private static RefNumSingleton _instance = new RefNumSingleton();


        //Made constructor private, so new instance of this class cannot be instantiated.
        private RefNumSingleton(){}

        public static RefNumSingleton getInstance()
        {
            return _instance;
        }
        public int genBookRef()
        {
            bookingRef++;
            return bookingRef;
        }
        public int genCustRef()
        {
            custRef++;
            return custRef;
        }


    }
}
