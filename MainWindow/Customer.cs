﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainWindowNamespace
{
    public class Customer : Person
    {
        private string address;
        private int custRefNum;

        public Customer()
        {
             
        }

        public string Address
        {
            get { return this.address; }
            set
            {
                if (value != null && value != "")
                    this.address = value;
                else throw new ArgumentException("Address is either empty or null");
            }
        }
        public int CustRefNum
        {
            get { return this.custRefNum; }
            set
            {
                if (value != 0)
                    this.custRefNum = value;
                else throw new ArgumentException("Customer Reference Number is null");             
            }
        }
    }
}
